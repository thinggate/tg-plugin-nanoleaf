build-rpi:
	mkdir -p ./build/
	env GOOS=linux GOARCH=arm GOARM=5 CGO_ENABLED="1" CC=arm-linux-gnueabi-gcc go build -buildmode=plugin -o ./build/rpi-nanoleaf.so -a
build:
	mkdir -p ./build/
	env GOOS=linux go build -buildmode=plugin -o ./build/linux-nanoleaf.so