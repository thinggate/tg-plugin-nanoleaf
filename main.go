package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/grandcat/zeroconf"
	"github.com/mitchellh/mapstructure"
	"github.com/roderm/audio-panel-nanoleaf/nanoleafDriver"
	"github.com/roderm/audio-panel/plugin/iface"
)

func main() {
	cmd := flag.String("cmd", "discover", "Command to execute")
	port := flag.Int("port", 16021, "Nanoleaf port")
	ip := flag.String("ip", "", "Lights IP address")
	flag.Parse()
	command := *cmd
	switch command {
	case "auth":
		if len(*ip) > 0 {
			getAuth(*ip, *port)
		} else {
			fmt.Println("No IP given")
		}
	case "discover":
		resolver, err := zeroconf.NewResolver(nil)
		if err != nil {
			log.Fatalln("Failed to initialize resolver:", err.Error())
		}
		entries := make(chan *zeroconf.ServiceEntry)
		go func(results <-chan *zeroconf.ServiceEntry) {
			for entry := range results {
				log.Println(entry)
			}
			log.Println("No more entries.")
		}(entries)
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
		defer cancel()
		err = resolver.Browse(ctx, "_nanoleafapi._tcp", "local.", entries)
		if err != nil {
			log.Fatalln("Failed to browse:", err.Error())
		}

		<-ctx.Done()
	default:
		fmt.Println("Unknown command")
	}

}
func getAuth(ip string, port int) {
	client := &http.Client{
		Timeout: time.Second * 40,
	}
	resp, err := client.Post(fmt.Sprintf("http://%s:%d/api/v1/new", ip, port), "application/json", strings.NewReader(""))
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(resp)
	}
}

func NewDriver(ctx context.Context, config interface{}, id string) (iface.IDevice, error) {
	var conf nanoleafDriver.NanoLeafConfig
	err := mapstructure.Decode(config, &conf)
	if err != nil {
		return nil, err
	}
	return nanoleafDriver.NewNanoleafDriver(ctx, conf, id), nil
}

// curl -d "{}" -H "Content-Type: application/json" -X POST http://192.168.178.38:16021/api/v1/new
