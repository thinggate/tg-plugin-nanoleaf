package nanoleafDriver

import (
	"context"
	"testing"
	"time"

	pb "github.com/roderm/audio-panel-protobuf/go/msg/device"
)

func getTestDriver(ctx context.Context) *NanoLeafDriver {
	return NewNanoleafDriver(
		ctx,
		NanoLeafConfig{
			DeviceAddress: "192.168.178.38",
			DevicePort:    "16021",
			AuthToken:     "P5xkuCDhmhkFooLxSyXluPIYFPZk18DF",
			Pullcycle:     5,
		},
		"test",
	)
}
func TestReceiveValue(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	d := getTestDriver(ctx)
	d.OnUpdate(func(u *pb.PropertyUpdate) {
		cancel()
	})
	select {
	case <-ctx.Done():
		return
	case <-time.After(time.Second * 20):
		t.Error("Timeout exeeded")
	}
}
func TestSetPower(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	d := getTestDriver(ctx)
	d.requestValues()
	dev := d.GetDevice()
	prop, _ := d.getProp(dev.Items[0], "on")
	err := d.setPower(dev.Items[0], !prop.GetBoolean())
	if err != nil {
		t.Error(err)
	}
	time.Sleep(time.Second * 2)
	d.setPower(dev.Items[0], !prop.GetBoolean())
	if err != nil {
		t.Error(err)
	}
	cancel()
	<-ctx.Done()
}

func TestSetBrightness(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	d := getTestDriver(ctx)
	d.requestValues()
	dev := d.GetDevice()
	prop, _ := d.getProp(dev.Items[0], "brightness")
	before := prop.GetNumber()
	err := d.setBrightness(dev.Items[0], 20)
	if err != nil {
		t.Error(err)
	}
	time.Sleep(time.Second * 2)
	d.setBrightness(dev.Items[0], before)
	if err != nil {
		t.Error(err)
	}
	cancel()
	<-ctx.Done()
}
