package nanoleafDriver

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"time"

	pb "github.com/roderm/audio-panel-protobuf/go/msg/device"
)

const apiPath = "api/v1"

type StateValue struct {
	Value bool `json:"value"`
}

type BrightnessValue struct {
	Value int `json:"value"`
	Max   int `json:"max"`
	Min   int `json:"min"`
}

type NanoLeafState struct {
	On         StateValue      `json:"on"`
	Brightness BrightnessValue `json:"brightness"`
	Hue        BrightnessValue `json:"hue"`
	Sat        BrightnessValue `json:"sat"`
	CT         BrightnessValue `json:"ct"`
	ColorMode  string          `json:"colorMode"`
}
type NanoLeafResponse struct {
	Name  string        `json:"name"`
	State NanoLeafState `json:"state"`
}
type NanoLeafConfig struct {
	DeviceAddress string
	DevicePort    string
	AuthToken     string
	Pullcycle     int
}
type NanoLeafDriver struct {
	cl     *http.Client
	ctx    context.Context
	conf   NanoLeafConfig
	light  *pb.Device
	update []func(*pb.PropertyUpdate)
}

func NewNanoleafDriver(ctx context.Context, config NanoLeafConfig, id string) *NanoLeafDriver {
	dr := &NanoLeafDriver{
		light: &pb.Device{
			Identifier: id,
			Items:      []*pb.Item{},
		},
		ctx: ctx,
		cl: &http.Client{
			Timeout: time.Second * 10,
		},
		conf: config,
	}
	go dr.startPulls()
	return dr
}

func (d *NanoLeafDriver) requestValues() error {
	r, err := d.cl.Get(fmt.Sprintf("%s/%s/%s", d.getAddr(), apiPath, d.conf.AuthToken))
	if err != nil {
		return err
	}
	return func(response *http.Response) error {
		defer response.Body.Close()
		var resp NanoLeafResponse
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return err
		}
		err = json.Unmarshal(contents, &resp)
		if err != nil {
			return err
		}
		d.checkUpdate(resp)
		return nil
	}(r)
}
func (d *NanoLeafDriver) startPulls() {
	for {
		select {
		case <-d.ctx.Done():
			return
		default:
			err := d.requestValues()
			if err != nil {
				fmt.Println(err)
			}
			time.Sleep(time.Second * time.Duration(d.conf.Pullcycle))
		}
	}
}

func (d *NanoLeafDriver) updateProp(item *pb.Item, cmd string, value interface{}) {
	noUpdate := true
	prop, err := d.getProp(item, cmd)
	if err != nil {
		prop = &pb.Property{
			Name: cmd,
		}
		item.Properties = append(item.Properties, prop)
	}
	switch reflect.TypeOf(value).Kind() {
	case reflect.Bool:
		noUpdate = noUpdate && prop.GetBoolean() == value.(bool)
		prop.Value = &pb.Property_Boolean{Boolean: value.(bool)}
	case reflect.Int:
		noUpdate = noUpdate && prop.GetNumber() == int64(value.(int))
		prop.Value = &pb.Property_Number{Number: int64(value.(int))}
	case reflect.String:
		noUpdate = noUpdate && prop.GetText() == value.(string)
		prop.Value = &pb.Property_Text{Text: value.(string)}
	}
	if !noUpdate {
		for _, fn := range d.update {
			go fn(&pb.PropertyUpdate{
				DeviceIdentifier: d.light.Identifier,
				ItemIdentifier:   item.Identifier,
				State:            prop,
			})
		}
	}
}
func (d *NanoLeafDriver) checkUpdate(n NanoLeafResponse) {
	var item *pb.Item
	item, err := d.getItem(n.Name)
	if err != nil {
		item = &pb.Item{
			Name: n.Name,
		}
		d.light.Items = append(d.light.Items, item)
	}

	d.updateProp(item, "on", n.State.On.Value)
	d.updateProp(item, "brightness", n.State.Brightness.Value)

}

func (d *NanoLeafDriver) getItem(item string) (*pb.Item, error) {
	for _, i := range d.light.Items {
		if item == i.Name {
			return i, nil
		}
	}
	return nil, fmt.Errorf("Item not found")
}
func (d *NanoLeafDriver) getProp(item *pb.Item, prop string) (*pb.Property, error) {
	for _, p := range item.Properties {
		if p.Name == prop {
			return p, nil
		}
	}
	return nil, fmt.Errorf("Property not found")
}
func (d *NanoLeafDriver) getAddr() string {
	return fmt.Sprintf("http://%s:%s", d.conf.DeviceAddress, d.conf.DevicePort)
}
func (d *NanoLeafDriver) setPower(item *pb.Item, on bool) error {
	type reqBody struct {
		On StateValue `json:"on"`
	}
	body := reqBody{
		On: StateValue{
			Value: on,
		},
	}
	_, err := d.put("state", body)
	if err != nil {
		return err
	}
	d.updateProp(item, "on", on)
	return nil
}

func (d *NanoLeafDriver) setBrightness(item *pb.Item, value int64) error {
	type BrightValue struct {
		Value    int32 `json:"value"`
		Duration int32 `json:"duration"`
	}
	type reqBody struct {
		Brightness BrightValue `json:"brightness"`
	}
	body := reqBody{
		Brightness: BrightValue{
			Value:    int32(value),
			Duration: 0,
		},
	}
	_, err := d.put("state", body)
	if err != nil {
		return err
	}
	d.updateProp(item, "brightness", value)
	return nil
}
func (d *NanoLeafDriver) setHue(item *pb.Item, value int64) error {
	type HueValue struct {
		Value int32 `json:"value"`
	}
	type reqBody struct {
		Hue HueValue `json:"hue"`
	}
	body := reqBody{
		Hue: HueValue{
			Value: int32(value),
		},
	}
	_, err := d.put("state", body)
	if err != nil {
		return err
	}
	d.updateProp(item, "hue", value)
	return nil
}
func (d *NanoLeafDriver) put(path string, body interface{}) (io.ReadCloser, error) {
	jsBody, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}
	r, err := http.NewRequest(
		"PUT",
		fmt.Sprintf("%s/%s/%s/%s", d.getAddr(), apiPath, d.conf.AuthToken, path),
		strings.NewReader(string(jsBody)),
	)
	if err != nil {
		return nil, err
	}
	r.ContentLength = int64(len(string(jsBody)))
	response, err := d.cl.Do(r)
	if err != nil {
		return nil, err
	} else {
		switch response.StatusCode {
		case http.StatusNoContent:
			return response.Body, nil
		case http.StatusUnauthorized:
			return nil, errors.New("Auth token is invalid")
		case http.StatusBadRequest:
			return nil, errors.New("Bad request => check body")
		case http.StatusNotFound:
			return nil, errors.New("Resource not found")
		default:
			return nil, errors.New("Unknown error")
		}
	}
}

func (d *NanoLeafDriver) PropertyUpdate(update *pb.PropertyUpdate) error {
	item, err := d.getItem(update.ItemIdentifier)
	if err != nil {
		return err
	}
	switch update.State.GetName() {
	case "on":
		return d.setPower(item, update.State.GetBoolean())
	case "brightness":
		return d.setBrightness(item, update.State.GetNumber())
	case "hue":
		return d.setHue(item, update.State.GetNumber())
	default:
		return fmt.Errorf("Device %s with Item %s doesn't support %s \n", update.DeviceIdentifier, update.ItemIdentifier, update.State.GetName())
	}
}
func (d *NanoLeafDriver) OnUpdate(cb func(*pb.PropertyUpdate)) {
	d.update = append(d.update, cb)
}

func (d *NanoLeafDriver) GetDevice() *pb.Device {
	return d.light
}
